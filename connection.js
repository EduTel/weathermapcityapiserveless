const { MongoClient } = require('mongodb');
const uri = `mongodb+srv://edutel:mimongo@cluster0.c4djo.mongodb.net/?retryWrites=true&w=majority`

const client = new MongoClient(uri,{ useUnifiedTopology: true });

module.exports = async () => {
    try {
        // Connect the client to the server
        await client.connect();
        // Establish and verify connection
        return client
    } catch (error) {
        console.log(error)
    }
};