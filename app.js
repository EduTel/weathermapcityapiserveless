//var express = require('express');
//var app = express();
const express = require('serverless-express/express')
var app = express();
const connection = require('./connection');


app.get('/', async function(req, res) {
    console.log("/")
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({"saludo":"hola"}));
});

app.get('/data1', async function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({"saludo":"data1"}));
});

app.get('/bd/:ciudad', async function(req, res) {
    const init = async () => {
        console.log("init")
        console.log("ciudad: "+req.params.ciudad)
        const client = await connection(); // obtenemos la conexión
        //console.log(database)
        const collection = await client.db("weather").collection('citys');
        const query = { "name": new RegExp(""+req.params.ciudad+"", 'i') };
        const cursor = await collection.find(query).sort({ length: -1 }).limit(10).toArray()
        console.log(typeof cursor)
        //await client.close();
        return cursor
        //cursor.forEach(doc => console.log(doc))
        //console.log("init end")
    };
    console.log(":inicio")
    cursor = await init()
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(cursor));
    console.log(":fin")
});
// set the port of our application
// process.env.PORT lets the port be set by Heroku
//var port = process.env.PORT || 3000;
//
//app.listen(port, function() {
//  console.log('Aplicación ejemplo, escuchando el puerto 3000!');
//});
module.exports = app